// JS COMMENTS
// They are important, acts as a quide for the programmer.
// Comments are disregarded no matter how long it may be.
	// Multi-Line Comments ctrl+shift+/

/*This 
is 
a 
multi-line 
comment*/

// STATEMENTS
// Programming Instruction that we tell the computer to perform.
// Statenebts usuakk end with semicolon (;)

// SYNTAX
// It is the set of rules that describes how statements must be constructed.



// alert("Hello Again!");

// This is a statement with a correct syntax
console.log("Hello World!");


// JS is a loose type programming language
console.log (" Hello World!  ");

// And also with this
console.
log
(
"Hello Again"
)

// [SECTION] Variables
// It is used as a container or storage

// Declaring Variables - tells our device that a variable name is created and is ready to store data.
// Syntax- let/const variableName;

// "let" is keyword that is usually used to declare a variable.

let myVariable;
let hello;

console.log(myVariable);
console.log(hello);

// = sign stand for intialization, that means giving a value to a variable.

// Guidlines for decalaring a variable
/*
1. use let keyword followed by varible name and then followed again by the assignment =.
2. variabl names should start with a lowercase letter.
3. for the constant cariable we are using const keyword.
4. variable names should be comprehensive or descriptive.
5. Do not use spaces on the declared variables.
6. Variable names must be unique
*/

// DIFFERENT CASING STYLES
// cammel case - thisIsCammelCasing
// snake case - this_is_snake_casing
// kebab case - this-is-kebab-casing

// this is a good variable name
let firstName = "Michael";  

// bad variable name
let pokemon = 25000;

// Declaring and initializing variables
// Syntax -> let/const variableName = value;

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);


// re-assigning a value to a variable.
productPrice = 25000;
console.log(productPrice);
console.log(productPrice);
console.log(productPrice);

let friend = "kate";
friend = "jane";
console.log(friend);

let supplier;
supplier = "John Smith Tradings";
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// let vs const
// let variables values can be changed, we can re-assign new values.
// const variables values cannot be changed

const hoursInAday = 24;
console.log(hoursInAday);

const pie = 3.14;
console.log(pie);

//  local/global scope variable

let outerVariable = "hi";   

{
 let innerVariable = "hello again";
 console.log(innerVariable);
console.log(outerVariable);

}

console.log(outerVariable);
// console.log(innerVariable); this will throw error, cause it is enclosed with braces

// MULTIPLE VARIABLE DECLARATION
// Multiple Variable may be declared in one line.
// Convenient, easier to read.

/*let productCode = "DC017";
let productBrand = "Dell";*/

let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);

// let is a reserved keyword,error
/*const let = "hello";
console.log(let);*/

// [SECTION] Data Types

// Strings - are series of characters that creates a word.

let country = "Philippines";
let	province = "Metro Manila";

// Concatenating String, using + symbol
// Combining string values

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);


// escape character (\)
//  "\n" refers to creating a new line between text

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John\'s employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

// Numbers
//  Itegers or Whole Numbers

let headcount = 26;
console.log(headcount);

// decimal or fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);




// Combining Text and Strings
console.log("John's grade last quarter is " + grade);

// Bollean
// we have 2 boolean values, true and false

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Arrays
// Are special kind of data type
// used to store multiple values with the same data types
// Syntax -> let/const arrayName = [elementA, elementB, ....]

let grades = 


let person = 


let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    contact: ["0123456789", "987654321"],
    address: {
        houseNumber: "345",
        city: "Manila"
    }
}

console.log(person);

let myGrades = {
    firstGrading: 98,
    secondGrading: 94,
    thirdGrading: 92,
    fourthGrading: 90
}

consolo.log(myGrades);

// checking data type
console.log(typeof grades);
console.log(typeof person);
console.log(typeof isMarried);

// in programming we always start counting from 0
const anime = ["one piece", "one punch man", "attack on titan"];
// anime = "kimetsu no yaiba";
anime[0] = "kimetsu no yaiba";

console.log(anime);

// Null vs Undefined
let spouse = 0; //Null/0/empty

let fullName;


















